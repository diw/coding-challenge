import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;

@SpringBootApplication
public class BestDayApplication {

    public static void main(String[] args) {

        SpringApplication.run(BestDayApplication.class, args);
    }
}


