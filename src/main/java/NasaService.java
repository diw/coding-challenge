import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;


public class NasaService {

    // ex: https://api.nasa.gov/neo/rest/v1/feed?start_date=2015-09-07&end_date=2015-09-08&api_key=DEMO_KEY

    private String apiKey = "H7V98iNStSoR1hRBQJgIMHWee0W6Pf5qvfyVLk4X";
    private String endpoint = "https://api.nasa.gov/neo/rest/v1/feed?";
    private double maxDiameterInKilometers = .5;

    public NasaService()
    {
    }

    /*
    * Enter a start and end date to find the day with the least amount of dangerous (> .5 kilometers in max approximate diameter)
    *
    * @param startdate	YYYY-MM-DD
    * @param enddate	YYYY-MM-DD
     */
    public String getBestDayToLaunch(String startdate, String enddate) {
        String bestDay = "";
        String date;
        int currentMinDangerousNeoCount = 9999;
        List<NasaApiResult.NearEarthObject> daysNeos;
        int daysDangerousNEOCount = 0;
        Map<String, List<NasaApiResult.NearEarthObject>> neos = getNEOs(startdate, enddate);

        for (Map.Entry<String, List<NasaApiResult.NearEarthObject>> daysNeoList : neos.entrySet()) {
            date = daysNeoList.getKey();
            if(!isDateInRange(date, startdate, enddate)) {
                break;
            }
            daysNeos = daysNeoList.getValue();
            String units;
            // loop through the days neos
            for (NasaApiResult.NearEarthObject neo : daysNeos) {
                //Map.Entry<String, NasaApiResult.Diameter>
                //neo.estimated_diameter
                if (isNeoDangerous(neo))
                    daysDangerousNEOCount++;
            }
            if (daysDangerousNEOCount < currentMinDangerousNeoCount) {
                currentMinDangerousNeoCount = daysDangerousNEOCount;
                bestDay = date;
                daysDangerousNEOCount = 0;
            }
        }
        return bestDay;
    }

    private boolean isDateInRange(String sDate, String sStartDate, String sEndDate) {
        Date date = new Date(sDate);
        Date startDate = new Date(sStartDate);
        Date endDate = new Date(sDate);

        return !(date.before(startDate) || date.after(endDate));
    }

    private boolean isNeoDangerous(NasaApiResult.NearEarthObject neo) {
        String units;
        NasaApiResult.Diameter diam;
        for (Map.Entry<String, NasaApiResult.Diameter> diameters: neo.estimated_diameter.entrySet() ) {
            units = diameters.getKey();
            if (units.equals("kilometers")) {
                 diam = diameters.getValue();
                 if (diam.estimated_diameter_max >  maxDiameterInKilometers) {
                     return true;
                 }
            }
        }
        return false;
    }

    /*
        Retrieve a Map of a list of NEOs keyed by date.
     */
    private Map<String, List<NasaApiResult.NearEarthObject>> getNEOs(String startdate, String enddate) {
        String url = endpoint + "startDate= " + startdate + "&enddate= " + enddate + "&apiKey=" + apiKey;

        Map<String, List<NasaApiResult.NearEarthObject>> neosByDate = consume(url);
        return neosByDate;
    }


    private static Map<String, List<NasaApiResult.NearEarthObject>> consume(String url) {

//        Client client = ClientBuilder.newClient();
//        client.register(JacksonJsonProvider.class);
          Map<String, List<NasaApiResult.NearEarthObject>> neosByDate = null;
//        Response response = client.target(url).request().get();
//        Map<String, List<NasaApiResult.NearEarthObject>> neosByDate = response.readEntity(NasaApiResult.NearEarthObject[].class);

        //using Jackson
        try {
           neosByDate = new ObjectMapper().readValue(url, HashMap.class);
        } catch (IOException ioe){
            System.out.println("Exception mapping NEO JSON to POJOs: " + ioe.getMessage());

        }

//        response.close();
//        client.close();

        return neosByDate;
    }
}
